#!/usr/bin/env bash

CMD=${0##*/}
if [ $# = 0 ]
then
    cat <<EOF
USAGE: 
 $CMD {start,stop,status}

EOF
 exit 1
fi

. /etc/rc.d/init.d/functions

cd {{ jboss_home }}
ip a

function jboss_start() {
  daemon {{ jboss_home }}/{{ jboss_ver }}/bin/domain.sh  \
  --domain-config domain-slave.xml \
  --host-config=host-slave.xml \
  -P file://{{ jboss_home}}/{{ jboss_ver }}/cluster/domain-slave.properties \
  < /dev/null &
}

function jboss_stop(){
  {{ jboss_home }}/{{ jboss_ver }}/bin/jboss-cli.sh \
  --controller=`facter ipaddress`:9999 \
  --connect --command=/host=`facter fqdn`:shutdown
}

function jboss_status(){
  ps auxwwww | grep [j]ava | grep --color=auto [j]boss
}

# MENU
case $1 in
    start)
        jboss_start
        ;;
    stop)
        jboss_stop
        ;;
    status)
        jboss_status
        ;;        
    *)
        echo ""
        echo "Unknown command: $1"
        exit 1
        ;;
esac